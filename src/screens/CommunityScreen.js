import PropTypes from 'prop-types'
import React, { Component } from 'react'
import MobileNavigation from '../components/MobileNavigation'
import CommunityCardContainer from '../containers/CommunityScreenContainer/CommunityCardContainer'
import DemoCommunityCardContainer from '../containers/CommunityScreenContainer/DemoCommunityCardContainer'

export default class GuideScreen extends Component {
  static propTypes = {
    tripName: PropTypes.string,
    destinationName: PropTypes.string,
    budget: PropTypes.number,
    numberOfNights: PropTypes.number,
    hotels: PropTypes.string,
    hotel: PropTypes.string,
    restaurants: PropTypes.string,
    restaurant: PropTypes.string,
    attractions: PropTypes.string,
    attraction: PropTypes.string
  }

  render() {
    return (
      <div>
        <h1>Community</h1>
        {this.renderTrips()}
        <MobileNavigation />
      </div>
    )
  }

  renderTrips() {
    return (
      <div>
        <h3>trips:</h3>
        <DemoCommunityCardContainer />
      </div>
    )
  }
}
