import PropTypes from 'prop-types'
import React, { Component } from 'react'
import MobileNavigation from '../components/MobileNavigation'
import DemoGuideCardContainer from '../containers/GuideScreenContainer/DemoGuideCardContainer'
import Zoom from 'react-reveal/Zoom'

export default class GuideScreen extends Component {
  static propTypes = {
    tripName: PropTypes.string,
    destinationName: PropTypes.string,
    budget: PropTypes.number,
    numberOfNights: PropTypes.number,
    hotels: PropTypes.string,
    hotel: PropTypes.string,
    restaurants: PropTypes.string,
    restaurant: PropTypes.string,
    attractions: PropTypes.string,
    attraction: PropTypes.string
  }

  render() {
    return (
      <div>
        <Zoom>
          <h1>Reiseführer</h1>
        </Zoom>
        {this.renderGuides()}
        <MobileNavigation />
      </div>
    )
  }

  renderGuides() {
    return (
      <div>
        <Zoom>
          <DemoGuideCardContainer />
        </Zoom>
      </div>
    )
  }
}
