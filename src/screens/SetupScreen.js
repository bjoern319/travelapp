import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Button from '../components/Button'
import Calendar from '../components/SetupScreenComponents/Calendar'
import MobileNavigation from '../components/MobileNavigation'
import TripNameInputContainer from '../containers/SetupScreenContainer/TripNameInputContainer'
import BudgetInputContainer from '../containers/SetupScreenContainer/BudgetInputContainer'
import DestinationInputContainer from '../containers/StartScreenContainer/DestinationNameInputContainer'
import DaysInputContainer from '../containers/SetupScreenContainer/DaysInputContainer'

export default class SetupScreen extends Component {
  static propTypes = {
    user: PropTypes.string,
    date: PropTypes.arrayOf(PropTypes.Object),
    tripName: PropTypes.string,
    destinationName: PropTypes.string,
    budget: PropTypes.any,
    numberOfNights: PropTypes.any,
    onAddTripName: PropTypes.string,
    onAddDestinationName: PropTypes.string,
    onAddBudget: PropTypes.any,
    onAddNumberOfNights: PropTypes.any
  }

  render() {
    return (
      <div>
        <h1>setup your trip</h1>
        <form>
          {this.renderTripName()}
          {this.renderDestinationName()}
          {this.renderBudget()}
          {this.renderNumberOfDays()}
          {this.renderCalendar()}
        </form>
        <MobileNavigation />
      </div>
    )
  }

  renderTripName() {
    const { tripName, onAddTripName } = this.props
    return tripName ? (
      <div data-test-Id="SetupScreen-tripNameRender">{tripName}</div>
    ) : (
      <TripNameInputContainer
        data-test-ID="SetupScreen-TripNameInput"
        onSubmit={onAddTripName}
      />
    )
  }

  renderDestinationName() {
    const { destinationName, onAddDestinationName } = this.props
    return destinationName ? (
      <div data-test-id="SetupScreen-DestinnationNameRender">
        {destinationName}
      </div>
    ) : (
      <DestinationInputContainer
        data-test-id="setupScreen-destinationNameInput"
        onSubmit={onAddDestinationName}
      />
    )
  }

  renderBudget() {
    const { budget, onAddBudget } = this.props
    return budget ? (
      <div data-test-id="SetupScreen-BudgetRender">{budget}</div>
    ) : (
      <BudgetInputContainer
        data-test-id="SetupScreen-BudgetInput"
        onSubmit={onAddBudget}
      />
    )
  }

  renderNumberOfDays() {
    const { numberOfNights, onAddNumberOfNights } = this.props
    return numberOfNights ? (
      <div data-test-id="SetupScreen-NumberOfDaysRender">{numberOfNights}</div>
    ) : (
      <DaysInputContainer
        data-test-id="SetupScreen-NumberOfDaysInput"
        onSubmit={onAddNumberOfNights}
      />
    )
  }

  renderCalendar() {
    return (
      <div>
        <Link to="/">
          <Button>zurück</Button>
        </Link>
        <Link to="/currenttrip">
          <Button>weiter</Button>
        </Link>
        <Calendar />
      </div>
    )
  }
}
