import PropTypes from 'prop-types'
import React, { Component } from 'react'
import MobileNavigation from '../components/MobileNavigation'
import TripCardHeaderContainer from '../containers/CurrentTripScreenContainer/TripCardHeaderContainer'
import TripCardContainer from '../containers/CurrentTripScreenContainer/TripCardContainer'
import HotelInputContainer from '../containers/CurrentTripScreenContainer/HotelInputContainer'
import RestaurantInputContainer from '../containers/CurrentTripScreenContainer/RestaurantInputContainer'
import AttractionInputContainer from '../containers/CurrentTripScreenContainer/AttractionInputContainer'
import HotelCostInputContainer from '../containers/CurrentTripScreenContainer/HotelCostInputContainer'
import RestaurantCostInputContainer from '../containers/CurrentTripScreenContainer/RestaurantCostInputContainer'
import AttractionCostInputContainer from '../containers/CurrentTripScreenContainer/AttractionCostInputContainer'

import Zoom from 'react-reveal/Zoom'
import styled from 'styled-components'

const StyledDiv = styled.div`
  font-weight: 700;
  margin-left: 20px;
  border-bottom: 1px solid;
  font-weight: 800;
  color: black;
`
const Tag = styled.div`
  display: inline-block;
  padding: 4px 10px;
  background: orange;
  color: black;
  font-weight: bold;
  border: 1px solid #ddd;
  border-radius: 8px;
  margin: 10px 10px 5px 5px;
`

const StyledBox = styled.div`
  background-color: #e0f2f1;
  width: 95vw;
  height: flex;
  padding-top: 5px;
`
const InnerDiv = styled.div`
  display: flex;
  padding-bottom: 4px;
`

export default class CurrentTripScreen extends Component {
  static propTypes = {
    tripName: PropTypes.string,
    destinationName: PropTypes.string,
    budget: PropTypes.number,
    hotel: PropTypes.string,
    onAddHotel: PropTypes.func,
    attraction: PropTypes.string,
    onAddAttraction: PropTypes.func,
    restaurant: PropTypes.string,
    onAddRestaurant: PropTypes.func,
    hotelCost: PropTypes.number,
    onAddHotelCost: PropTypes.func,
    restaurantCost: PropTypes.number,
    onAddRestaurantCost: PropTypes.func,
    attractionCost: PropTypes.number,
    onAddAttractionCost: PropTypes.func,
    restBudget: PropTypes.number
  }

  render() {
    return (
      <div>
        <Zoom>
          <h1>deine Reise</h1>
        </Zoom>
        <Zoom>
          {this.renderCurrentTripDetails()}
          <StyledBox>
            {this.renderHotelOrInput()}
            {this.renderRestaurantsAndInput()}
            {this.renderAttractionInputAndAttractions()}
          </StyledBox>
        </Zoom>
        <MobileNavigation />
      </div>
    )
  }

  renderCurrentTripDetails() {
    return (
      <div>
        <TripCardHeaderContainer />
      </div>
    )
  }
  renderHotelOrInput() {
    const { hotel, onAddHotel } = this.props
    return hotel ? (
      <div>
        <Tag>Hotel:</Tag>
        <StyledDiv data-test-id="SetupScreen-destinationNameRender">
          <InnerDiv>
            <div>{hotel}</div>
            <div>{this.renderHotelCostInputOrHotelCosts()}</div>
          </InnerDiv>
        </StyledDiv>
      </div>
    ) : (
      <HotelInputContainer
        data-test-id="SetupScreen-DestinationNameInput"
        onSubmit={onAddHotel}
      />
    )
  }

  renderRestaurantsAndInput() {
    const { restaurant, onAddRestaurant } = this.props
    return restaurant ? (
      <div>
        <Tag>Restaurants:</Tag>
        <StyledDiv data-test-id="SetupScreen-destinationNameRender">
          <InnerDiv>
            <div>{restaurant}</div>
            <div>{this.renderRestaurantCostInputOrRestaurantCosts()}</div>
          </InnerDiv>
        </StyledDiv>
        <RestaurantInputContainer
          data-test-id="SetupScreen-DestinationNameInput"
          onSubmit={onAddRestaurant}
        />
      </div>
    ) : (
      <RestaurantInputContainer
        data-test-id="SetupScreen-DestinationNameInput"
        onSubmit={onAddRestaurant}
      />
    )
  }

  renderAttractionInputAndAttractions() {
    const { attraction, onAddAttraction } = this.props
    return attraction ? (
      <div>
        <Tag>Attractions:</Tag>
        <StyledDiv data-test-id="SetupScreen-destinationNameRender">
          <InnerDiv>
            <div>{attraction}</div>
            <div>{this.renderAttractionCostInputOrAttractionCosts()}</div>
          </InnerDiv>
        </StyledDiv>
        <AttractionInputContainer
          data-test-id="SetupScreen-DestinationNameInput"
          onSubmit={onAddAttraction}
        />
      </div>
    ) : (
      <AttractionInputContainer
        data-test-id="SetupScreen-DestinationNameInput"
        onSubmit={onAddAttraction}
      />
    )
  }

  renderHotelCostInputOrHotelCosts() {
    const { hotelCost, onAddHotelCost } = this.props
    return hotelCost ? (
      <div>
        <StyledDiv data-test-id="SetupScreen-destinationNameRender">
          {hotelCost}
          &nbsp;€
        </StyledDiv>
      </div>
    ) : (
      <HotelCostInputContainer
        data-test-id="SetupScreen-DestinationNameInput"
        onSubmit={onAddHotelCost}
      />
    )
  }

  renderRestaurantCostInputOrRestaurantCosts() {
    const { restaurantCost, onAddRestaurantCost } = this.props
    return restaurantCost ? (
      <div>
        <StyledDiv data-test-id="SetupScreen-destinationNameRender">
          {restaurantCost}
          &nbsp;€
        </StyledDiv>
      </div>
    ) : (
      <RestaurantCostInputContainer
        data-test-id="SetupScreen-DestinationNameInput"
        onSubmit={onAddRestaurantCost}
      />
    )
  }

  renderAttractionCostInputOrAttractionCosts() {
    const { attractionCost, onAddAttractionCost } = this.props
    return attractionCost ? (
      <div>
        <StyledDiv data-test-id="SetupScreen-destinationNameRender">
          {attractionCost}
          &nbsp;€
        </StyledDiv>
      </div>
    ) : (
      <AttractionCostInputContainer
        data-test-id="SetupScreen-DestinationNameInput"
        onSubmit={onAddAttractionCost}
      />
    )
  }
}
