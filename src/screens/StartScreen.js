import PropTypes from 'prop-types'
import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Calendar from '../components/SetupScreenComponents/Calendar'
import MobileNavigation from '../components/MobileNavigation'
import UserInputContainer from '../containers/StartScreenContainer/UserInputContainer'
import DestinationNameInputContainer from '../containers/StartScreenContainer/DestinationNameInputContainer'
import TripNameInputContainer from '../containers/SetupScreenContainer/TripNameInputContainer'
import BudgetInputContainer from '../containers/SetupScreenContainer/BudgetInputContainer'
import DaysInputContainer from '../containers/SetupScreenContainer/DaysInputContainer'
import Button from '../components/Button'
import styled from 'styled-components'
import Zoom from 'react-reveal/Zoom'

const StyledDiv = styled.div`
  font-weight: 700;
  margin-left: 20px;
`

export default class StartScreen extends Component {
  static propTypes = {
    user: PropTypes.string,
    onAddUser: PropTypes.func,
    tripName: PropTypes.string,
    onAddTripName: PropTypes.func,
    destinationName: PropTypes.string,
    onAddDestinationName: PropTypes.func,
    budget: PropTypes.number,
    onAddBudget: PropTypes.func,
    numberOfNights: PropTypes.number,
    onAddNumberOfNights: PropTypes.func,
    date: PropTypes.arrayOf(PropTypes.Object),
    onClick: PropTypes.func,
    onAddHotel: PropTypes.func,
    hotel: PropTypes.string,
    restBudget: PropTypes.number
  }

  state = {
    hidden: true
  }

  render() {
    return (
      <div>
        <Zoom>
          <h1>Wilkommen!</h1>
        </Zoom>
        {this.renderUserOrInput()}
        {this.renderNewTrip()}
        {this.renderToggleSection()}
        <MobileNavigation />
      </div>
    )
  }

  toggleInput = () => {
    this.setState({
      hidden: !this.state.hidden
    })
  }

  renderNewTrip() {
    return (
      <div>
        <h3>füge eine neue Reise hinzu</h3>
        <Button onClick={this.toggleInput}>+</Button>
      </div>
    )
  }

  renderToggleSection() {
    return this.state.hidden ? (
      ''
    ) : (
      <section>
        <Zoom>
          <div>{this.renderTripName()}</div>
          <div>{this.rederDestinationOrInput()}</div>
          <div>{this.renderBudget()}</div>
          <div>{this.renderNumberOfDays()}</div>
          <div>{this.renderCalendar()}</div>
        </Zoom>
      </section>
    )
  }

  renderUserOrInput() {
    const { user, onAddUser } = this.props
    return user ? (
      <StyledDiv data-test-id="StartScreen-UserRender">{user}</StyledDiv>
    ) : (
      <UserInputContainer
        data-test-id="StartScreen-UserInput"
        onSubmit={onAddUser}
      />
    )
  }

  renderTripName() {
    const { tripName, onAddTripName } = this.props
    return tripName ? (
      <StyledDiv data-test-id="SetupScreen-tripNameRender">
        {tripName}
      </StyledDiv>
    ) : (
      <TripNameInputContainer
        data-test-id="SetupScreen-TripNameInput"
        onSubmit={onAddTripName}
      />
    )
  }

  rederDestinationOrInput() {
    const { destinationName, onAddDestinationName } = this.props
    return destinationName ? (
      <StyledDiv data-test-id="SetupScreen-destinationNameRender">
        {destinationName}
      </StyledDiv>
    ) : (
      <DestinationNameInputContainer
        data-test-id="SetupScreen-DestinationNameInput"
        onSubmit={onAddDestinationName}
      />
    )
  }

  renderBudget() {
    const { budget, onAddBudget } = this.props
    return budget ? (
      <StyledDiv data-test-id="SetupScreen-BudgetRender">
        {budget}
        &nbsp;€
      </StyledDiv>
    ) : (
      <BudgetInputContainer
        data-test-id="SetupScreen-BudgetInput"
        onSubmit={onAddBudget}
      />
    )
  }

  renderNumberOfDays() {
    const { numberOfNights, onAddNumberOfNights } = this.props
    return numberOfNights ? (
      <StyledDiv data-test-id="SetupScreen-NumberOfDaysRender">
        {numberOfNights}
        &nbsp;Nächte
      </StyledDiv>
    ) : (
      <DaysInputContainer
        data-test-id="SetupScreen-NumberOfDaysInput"
        onSubmit={onAddNumberOfNights}
      />
    )
  }

  renderCalendar() {
    return (
      <div>
        <Link to="/">
          <Button>zurück</Button>
        </Link>
        <Link to="/currenttrip">
          <Button>weiter</Button>
        </Link>
        <Calendar />
      </div>
    )
  }
}
