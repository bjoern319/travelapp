import React, { Component } from 'react'
import MobileNavigation from '../components/MobileNavigation'
import MapContainer from '../containers/MapScreenContainer/MapContainer'

import Zoom from 'react-reveal/Zoom'

export default class MapScreen extends Component {
  render() {
    return (
      <div>
        <Zoom>
          <h1>Explore your Destination!</h1>
        </Zoom>
        {this.renderPictures()}
        <MobileNavigation />
      </div>
    )
  }

  renderPictures() {
    return (
      <div>
        <MapContainer />
      </div>
    )
  }
}
