import React, { Component } from 'react'
import MobileNavigation from '../components/MobileNavigation'
import PhotosContainer from '../containers/ExploreScreenContainer/PhotosContainer'

import Zoom from 'react-reveal/Zoom'

export default class ExploreScreen extends Component {
  render() {
    return (
      <div>
        <Zoom>
          <h1>Explore your Destination!</h1>
        </Zoom>
        {this.renderPictures()}
        <MobileNavigation />
      </div>
    )
  }

  renderPictures() {
    return (
      <div>
        <PhotosContainer />
      </div>
    )
  }
}
