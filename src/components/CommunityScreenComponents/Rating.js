import React, { Component } from 'react'
import Star from './Star'

export default class Rating extends Component {
  render() {
    const { callback, rating } = this.props
    ;<React.Fragment>{this.renderStars()}</React.Fragment>

    return Array(5)
      .fill()
      .map((_, index) => (
        <Star onClick={() => callback(index + 1)} filled={index < rating} />
      ))
  }

  renderStars() {
    const { callback, rating } = this.props
  }
}
