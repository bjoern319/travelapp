import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const Wrapper = styled.section`
  display: flex;
`

const StyledSection = styled.section`
  max-height: flex;
  min-height: 150px;
  width: 95vw;
  margin: 15px;
  display: flex;
  flex-direction: column;
  padding-top: 10px;
  background-color: #efefef;
  color: black;
`

const StyledWrapper = styled.section`
  display: flex;
  background-color
`

const StyledTitle = styled.div`
  display: inline-block;
  padding: 4px 10px;
  background: limegreen;
  width: 80px;
  color: black;
  font-weight: bold;
  border: 1px solid #ddd;
  border-radius: 8px;
  margin: 10px 10px 5px 0;
`

const StyledDestinationName = styled.div`
  display: inline-block;
  padding: 4px 10px;
  background: orange;
  color: black;
  font-weight: bold;
  border: 1px solid #ddd;
  border-radius: 8px;
  margin: 10px 10px 5px 0;
`

const StyledBudget = styled.div`
  display: inline-block;
  padding: 4px 10px;
  background: orange;
  color: black;
  font-weight: bold;
  border: 1px solid #ddd;
  border-radius: 8px;
  margin: 10px 10px 5px 0;
`

const StyledHotel = styled.div`
  font-weight: 600;
  margin-left: 10px;
`

const StyledRestaurant = styled.div`
  font-weight: 600;
  margin-left: 10px;
`

const StyledAttraction = styled.div`
  font-weight: 600;
  margin-left: 10px;
`

export default class DemoCommunityCard extends Component {
  static propTypes = {
    trip: PropTypes.Object,
    destinationName: PropTypes.string,
    budget: PropTypes.number
  }

  render() {
    return (
      <Wrapper>
        <StyledSection>
          <StyledTitle>Asien</StyledTitle>
          <StyledWrapper>
            <StyledDestinationName>Destination: Bangkok</StyledDestinationName>
            <StyledBudget>Budget: 3000€</StyledBudget>
          </StyledWrapper>
          <StyledHotel>Hotel: Tower Club at Lebua</StyledHotel>
          <StyledRestaurant>Restaurant: Lung Ja Hua Hin</StyledRestaurant>
          <StyledAttraction>Attraction: Golden Mountain</StyledAttraction>
        </StyledSection>
      </Wrapper>
    )
  }
}
