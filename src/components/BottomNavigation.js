import React, { Component } from 'react'
import styled from 'styled-components'
import { NavLink } from 'react-router-dom'
import Button from './Button'

const StyledNav = styled.nav`
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: blue;
  padding: 5px 0px;
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 1;
  -webkit-overflow-scrolling: touch;
  overflow-x: scroll;
`
const NavButton = styled(Button)``

export default class BottomNavigation extends Component {
  render() {
    return (
      <StyledNav>
        <NavLink to="/">
          <NavButton>start</NavButton>
        </NavLink>
        <NavLink to="/currenttrip">
          <NavButton>currentTrip</NavButton>
        </NavLink>
        <NavLink to="/explore">
          <NavButton>explore</NavButton>
        </NavLink>
        <NavLink to="/guide">
          <NavButton>Reiseführer</NavButton>
        </NavLink>
        <NavLink to="/community">
          <NavButton>community</NavButton>
        </NavLink>
      </StyledNav>
    )
  }
}
