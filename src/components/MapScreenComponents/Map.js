import React, { Component } from 'react'
import styled from 'styled-components'
import Zoom from 'react-reveal/Zoom'

const Wrapper = styled.section`
  display: flex;
  justify-content: center;
  max-width: 375px;
`

export default class Map extends Component {
  render() {
    return (
      <div>
        <Zoom>
          <Wrapper>
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1935014.3829878962!2d97.66841109318175!3d18.694263811547806!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30d0ccc8c2997841%3A0x6a69e176922503d4!2sChiang+Mai%2C+Thailand!5e0!3m2!1sde!2sde!4v1540281000068"
              width="600"
              height="450"
              frameborder="0"
              allowfullscreen
              title="Map"
            />
          </Wrapper>
        </Zoom>
      </div>
    )
  }
}
