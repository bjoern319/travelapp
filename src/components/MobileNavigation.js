import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import BottomNavigation from '@material-ui/core/BottomNavigation'
import BottomNavigationAction from '@material-ui/core/BottomNavigationAction'
import MapIcon from '@material-ui/icons/Map'
import PhotoIcon from '@material-ui/icons/CameraAlt'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import YourTripIcon from '@material-ui/icons/Flight'
import GuideIcon from '@material-ui/icons/Panorama'
import { NavLink } from 'react-router-dom'

import styled from 'styled-components'

const StyledNav = styled.nav`
  padding: 0px 0px;
  position: fixed;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 1;
`

const styles = {
  root: {}
}

class MobileNavigation extends React.Component {
  state = {
    value: 'Start'
  }

  handleChange = (event, value) => {
    this.setState({ value })
  }

  render() {
    const { value } = this.state

    return (
      <StyledNav>
        <BottomNavigation value={value} onChange={this.handleChange}>
          <NavLink to="/">
            <BottomNavigationAction
              label="Start"
              value="Start"
              icon={<AddCircleIcon />}
            />
          </NavLink>
          <NavLink to="/currenttrip">
            <BottomNavigationAction
              label="current Trip"
              value="currentTrip"
              icon={<YourTripIcon />}
            />
          </NavLink>
          <NavLink to="/explore">
            <BottomNavigationAction
              label="explore"
              value="explore"
              icon={<PhotoIcon />}
            />
          </NavLink>
          <NavLink to="/guide">
            <BottomNavigationAction
              label="guide"
              value="guide"
              icon={<GuideIcon />}
            />
          </NavLink>
          <NavLink to="/map">
            <BottomNavigationAction
              label="map"
              value="map"
              icon={<MapIcon />}
            />
          </NavLink>
        </BottomNavigation>
      </StyledNav>
    )
  }
}

MobileNavigation.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(MobileNavigation)
