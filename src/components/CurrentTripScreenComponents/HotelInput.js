import Input from '../Input'
import styled from 'styled-components'

const HotelInput = styled(Input)`
  background: lightblue !important;
  color: white;
  font-size: 2em;
`

export default HotelInput
