import Input from '../Input'
import styled from 'styled-components'

const RestaurantInput = styled(Input)`
  background: lightblue !important;
  color: white;
  font-size: 2em;
`

export default RestaurantInput
