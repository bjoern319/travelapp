import React, { Component } from 'react'
import styled from 'styled-components'

const Wrapper = styled.section`
  max-height: flex;
  width: 95vw;
  display: flex;
  align-items: center;
  flex-direction: column;
  padding-top: 5px;
  padding-bottom: 5px;
  background-color: #80cbc4;
`
const StyledSection = styled.section`
  display: flex;
`

const StyledTitle = styled.p`
  font-weight: 800;
  text-transform: uppercase;
`
const StyledInfo = styled.p`
  font-weight: 600;
`
const Tag = styled.div`
  display: inline-block;
  padding: 5px 5px;
  background: orange;
  color: black;
  font-weight: bold;
  border: 1px solid #ddd;
  border-radius: 8px;
  margin: 5px 5px 5px 5px;
`
const BudgetTag = styled.div`
  display: inline-block;
  padding: 5px 5px;
  background: red;
  color: white;
  font-weight: bold;
  border: 1px solid #ddd;
  border-radius: 8px;
  margin: 5px 5px 5px 5px;
`

export default class TripCardHeader extends Component {
  render() {
    const { tripName, destinationName, budget, restBudget } = this.props
    return (
      <Wrapper>
        <Tag>
          <StyledSection>
            <StyledTitle>{tripName}: &nbsp;</StyledTitle>
            <StyledInfo>
              Ziel:&nbsp;
              {destinationName}, &nbsp;
            </StyledInfo>
            <StyledInfo>
              Budget:&nbsp;
              {budget}€
            </StyledInfo>
          </StyledSection>
        </Tag>
        <BudgetTag>
          Restliches Budget:&nbsp;
          {restBudget}€
        </BudgetTag>
      </Wrapper>
    )
  }
}
