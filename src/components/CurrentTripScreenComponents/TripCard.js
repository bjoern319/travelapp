import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const StyledSubHeader = styled.section`
  display: flex;
  background-color: lightgray;
  width: 95vw;
  height: flex;
`
const DayBox = styled.div`
  max-height: 20px;
  max-width: 20 px;
  background-color: hotpink;
`

export default class TripCard extends Component {
  static propTypes = {
    numberOfNights: PropTypes.number,
    hotels: PropTypes.any,
    restaurants: PropTypes.any,
    attractions: PropTypes.any,
    hotel: PropTypes.any
  }

  render() {
    return <StyledSubHeader>{this.renderBoxes()}</StyledSubHeader>
  }

  renderBoxes() {
    const numberOfNights = this.props
    let index = 0
    if (index <= numberOfNights) {
      index = index++
      return (
        <div>
          <DayBox>test</DayBox>
        </div>
      )
    }
  }
}
