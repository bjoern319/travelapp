import Input from '../Input'
import styled from 'styled-components'

const CostInput = styled(Input)`
  background: lightblue !important;
  color: white;
  font-size: 1.5em;
  height: 24px;
  max-width: 100px;
  margin-left: 20px;
  paddingleft: 5px;
`

export default CostInput
