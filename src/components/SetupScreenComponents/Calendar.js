import React, { Component } from 'react'
import Calendar from 'react-calendar'

import styled from 'styled-components'

const Wrapper = styled.section`
  display: flex;
  justify-content: center;
  font-size: 1.5em;
  margin-bottom: 70px;
`

export default class MyApp extends Component {
  state = {
    date: new Date()
  }

  onChange = date => this.setState({ date })

  render() {
    return (
      <Wrapper>
        <Calendar onChange={this.onChange} value={this.state.date} />
      </Wrapper>
    )
  }
}
