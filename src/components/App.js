import React, { Component } from 'react'
import { Provider } from 'react-redux'
import StartScreenContainer from '../containers/StartScreenContainer/StartScreenContainer'
import ExploreScreenContainer from '../containers/ExploreScreenContainer/ExploreScreenContainer'
import CommunityScreenContainer from '../containers/CommunityScreenContainer/CommunityScreenContainer'
import CurrentTripScreenContainer from '../containers/CurrentTripScreenContainer/CurrentTripScreenContainer'
import GuideScreenContainer from '../containers/GuideScreenContainer/GuideScreenContainer'
import MapScreenContainer from '../containers/MapScreenContainer/MapScreenContainer'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { saveToLocalStorage } from '../middlewares'
import reducer from '../reducer'
import thunk from 'redux-thunk'
import { loadUsersFromServer } from '../actions'
import { applyMiddleware, createStore } from 'redux'

const store = createStore(
  reducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(saveToLocalStorage, thunk)
)

class App extends Component {
  componentDidMount() {
    store.dispatch(loadUsersFromServer())
  }

  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <Route exact path="/" component={StartScreenContainer} />
            <Route path="/explore" component={ExploreScreenContainer} />
            <Route path="/currenttrip" component={CurrentTripScreenContainer} />
            <Route path="/guide" component={GuideScreenContainer} />
            <Route path="/community" component={CommunityScreenContainer} />
            <Route path="/map" component={MapScreenContainer} />
          </div>
        </Router>
      </Provider>
    )
  }
}

export default App
