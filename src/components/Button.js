import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const StyledButton = styled.button`
  height: 30px;
  max-width: 108px;
  margin: 5px;
  border: 2px solid;
  border-color: white;
  color: white;
  outline: none;
  cursor: pointer;
  background: blue;
  transition: 0.5s;
  font-size: 15px;
  border-radius: 5px;

  &:hover {
    border: none;
    color: blue;
    font-size: 20px;
    transform: scale(1.1);
    background: white;
    border-color: white;
    transition: all 0.3s ease;
  }

  &:active {
    color: white;
    background: red;
  }
`

export default class Button extends Component {
  static propTypes = {
    color: PropTypes.string,
    children: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.string,
      PropTypes.node
    ]),
    dataTestId: PropTypes.string,
    onClick: PropTypes.func
  }

  render() {
    const { color, dataTestId } = this.props
    return (
      <StyledButton
        data-test-id={dataTestId}
        color={color}
        onClick={this.props.onClick}
      >
        {this.props.children}
      </StyledButton>
    )
  }
}
