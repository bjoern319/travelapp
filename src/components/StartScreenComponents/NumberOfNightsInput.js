import Input from '../Input'
import styled from 'styled-components'

const NumberOfNightsInput = styled(Input)`
  background: lightgray !important;
  color: white;
  font-size: 2em;
`

export default NumberOfNightsInput
