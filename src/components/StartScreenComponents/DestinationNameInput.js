import Input from '../Input'
import styled from 'styled-components'

const DestinationNameInput = styled(Input)`
  background: lightgreen !important;
  color: white;
  font-size: 2em;
`

export default DestinationNameInput
