import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const Wrapper = styled.section`
  display: flex;
  margin-bottom: 80px;
`

const StyledSection = styled.section`
  max-height: flex;
  min-height: 150px;
  width: 95vw;
  margin: 15px;
  display: flex;
  flex-direction: column;
  padding-top: 10px;
  background-color: #efefef;
  color: black;
`

const StyledWrapper = styled.section`
  display: flex;
  background-color
`

const StyledTitle = styled.div`
  display: inline-block;
  padding: 4px 10px;
  background: limegreen;
  width: 80px;
  color: black;
  font-weight: bold;
  border: 1px solid #ddd;
  border-radius: 8px;
  margin: 10px 10px 5px 0;
`

const StyledDestinationName = styled.div`
  display: inline-block;
  padding: 4px 10px;
  background: orange;
  color: black;
  font-weight: bold;
  border: 1px solid #ddd;
  border-radius: 8px;
  margin: 10px 10px 5px 0;
`

const StyledBudget = styled.div`
  display: inline-block;
  padding: 4px 10px;
  background: orange;
  color: black;
  font-weight: bold;
  border: 1px solid #ddd;
  border-radius: 8px;
  margin: 10px 10px 5px 0;
`

const StyledHotel = styled.div`
  font-weight: 600;
  margin-left: 10px;
`

const StyledRestaurant = styled.div`
  font-weight: 600;
  margin-left: 10px;
`

const StyledAttraction = styled.div`
  font-weight: 600;
  margin-left: 10px;
`

const StyledText = styled.div`
  font-weight: 600;
  margin-left: 10px;
`

const StyledRating = styled.div`
  font-weight: 600;
  margin-left: 10px;
`

const StyledGuideTag = styled.div`
display: inline-block;
  padding: 4px 10px;
  background: orange;
  color: black;
  font-weight: bold;
  border: 1px solid #ddd;
  border-radius: 8px;
  margin: 10px 10px 5px 0;
  width: 80px;
`

export default class DemoCommunityCard extends Component {
  static propTypes = {
    trip: PropTypes.Object,
    destinationName: PropTypes.string,
    budget: PropTypes.number
  }

  render() {
    return (
      <Wrapper>
        <StyledSection>
          <StyledTitle>Asien</StyledTitle>
          <StyledWrapper>
            <StyledDestinationName>Destination: Bangkok</StyledDestinationName>
            <StyledBudget>Budget: 3000€</StyledBudget>
          </StyledWrapper>
          <StyledHotel>Hotel: Tower Club at Lebua</StyledHotel>
          <StyledRestaurant>Restaurant: Lung Ja Hua Hin</StyledRestaurant>
          <StyledAttraction>Attraction: Golden Mountain</StyledAttraction>
          <StyledGuideTag>Guide:</StyledGuideTag>
          <StyledText>
            <p>Bangkok ist seit 1782 die Hauptstadt des Königreichs Thailand. Sie hat einen Sonderverwaltungsstatus und wird von einem Gouverneur regiert. Die Hauptstadt hat 8,249 Millionen Einwohner (Volkszählung 2010) und ist die mit Abstand größte Stadt des Landes. In der Bangkok Metropolitan Region (BMR), der größten Metropolregion in Thailand, leben insgesamt 14,566 Millionen Menschen (Volkszählung 2010).
            </p>
            <p>
            Die Stadt ist das politische, wirtschaftliche und kulturelle Zentrum Thailands mit Universitäten, Hochschulen, Palästen und über 400 Wats (buddhistische Tempelanlagen und Klöster) sowie wichtigster Verkehrsknotenpunkt des Landes. Das Nationalmuseum in Bangkok ist das größte seiner Art in Südostasien. In Bangkok ist auch die Wirtschafts- und Sozialkommission für Asien und den Pazifik (UNESCAP) beheimatet. Mit mehr als 17 Millionen ausländischen Touristen war Bangkok im Jahr 2013 die meistbesuchte Stadt der Welt, bevor sie 2014 wieder von London abgelöst wurde und seither auf Platz 2 rangiert.[3] In den Jahren 2016 und 2017 stand Bangkok mit 21,5 Millionen bzw. 20,2 Millionen Touristen wieder auf Platz 1 der meistbesuchten Städte der Welt.
            </p>
          </StyledText>
          <StyledRating> Rating: 10/10</StyledRating>
        </StyledSection>
      </Wrapper>
    )
  }
}
