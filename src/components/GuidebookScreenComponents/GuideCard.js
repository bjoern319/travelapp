import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const Wrapper = styled.section`
  display: flex;
`

const StyledSection = styled.section`
  max-height: flex;
  min-height: 150px;
  width: 95vw;
  margin: 15px;
  display: flex;
  flex-direction: column;
  padding-top: 10px;
  background-color: lightgray;
`
const StyledTitle = styled.p`
  margin: 10px 0 0;
  font-weight: 800;
`
export default class CommunityCard extends Component {
  static propTypes = {
    trip: PropTypes.Object,
    destinationName: PropTypes.string,
    budget: PropTypes.number
  }

  render() {
    return (
      <Wrapper>
        <StyledSection>
          <StyledTitle>{this.tripName}</StyledTitle>
          <div>test</div>
          <div>{this.destinationName}</div>
          <div>{this.budget}</div>
          <div>{this.hotels}</div>
          <div>{this.restaurants}</div>
          <div>{this.attractions}</div>
          <div>text</div>
          <div>{this.renderRating()}</div>
        </StyledSection>
      </Wrapper>
    )
  }

  renderRating() {}
}
