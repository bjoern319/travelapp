import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const Wrapper = styled.section`
  display: flex;
  justify-content: center;
`
const StyledSection = styled.section`
  display: flex;
  flex-direction: column;
  max-width: 375px;
  margin-bottom: 70px;
`

export default class Photos extends Component {
  static propTypes = {
    user: PropTypes.string,
    destinationName: PropTypes.string
  }

  state = {
    pictures: []
  }

  render() {
    return (
      <div>
        <Wrapper>
          <StyledSection>{this.state.pictures}</StyledSection>
        </Wrapper>
      </div>
    )
  }
  componentDidMount() {
    const fotos = this.props.destinationName || 'chiangmai'
    const apiKey = 'b593ed983ef7011f291fee0d45eb91b3'
    fetch(
      'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=' +
        apiKey +
        '&tags=' +
        fotos +
        '&per_page=100&page=1&format=json&nojsoncallback=1'
    )
      .then(function(response) {
        return response.json()
      })
      .then(
        function(j) {
          let picArray = j.photos.photo.map(pic => {
            let srcPath =
              'https://farm' +
              pic.farm +
              '.staticflickr.com/' +
              pic.server +
              '/' +
              pic.id +
              '_' +
              pic.secret +
              '.jpg'
            return <img alt="pictures" src={srcPath} />
          })
          this.setState({ pictures: picArray })
        }.bind(this)
      )
  }
}
