import React, { Component } from 'react'
import styled from 'styled-components'
import PropTypes from 'prop-types'

const styledBox = styled.div`
  width: 200px;
  background-color: lightgray;

  @media (max-width: 1200px) {
    width: 600px;
  }
`
