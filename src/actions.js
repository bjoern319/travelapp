import { createAction } from 'redux-actions'
import { loadUsers } from './services'

const ACTIONS = {
  ADD_USER: 'ADD_USER',
  SAVE_DATE: 'SAVE_DATE',
  SAVE_TRIP_NAME: 'SAVE_TRIP_NAME',
  SAVE_TRIP: 'SAVE_TRIP',
  REPLACE_USERS: 'REPLACE_USERS',
  ADD_TRIP_NAME: 'ADD_TRIP_NAME',
  ADD_START_DATE: 'ADD_START_DATE',
  ADD_END_DATE: 'ADD_END_DATE',
  ADD_DESTINATION_NAME: 'ADD_DESTINATION_NAME',
  ADD_BUDGET: 'ADD_BUDGET',
  ADD_NUMBER_OF_NIGHTS: 'ADD_NUMBER_OF_NIGHTS',
  ADD_HOTEL: 'ADD_HOTEL',
  ADD_RESTAURANT: 'ADD_RESTAURANT',
  ADD_ATTRACTION: 'ADD_ATTRACTION',
  ADD_HOTEL_COST: 'ADD_HOTEL_COST',
  ADD_RESTAURANT_COST: 'ADD_RESTAURANT_COST',
  ADD_ATTRACTION_COST: 'ADD_ATTRACTION_COST',
  LIST_PAST_VACATIONS: 'LIST_PAST_VACATIONS'
}

export const addNumberOfNights = createAction(ACTIONS.ADD_NUMBER_OF_NIGHTS)
export const addBudget = createAction(ACTIONS.ADD_BUDGET)
export const addDestinationName = createAction(ACTIONS.ADD_DESTINATION_NAME)
export const addTripName = createAction(ACTIONS.ADD_TRIP_NAME)
export const addStartDate = createAction(ACTIONS.ADD_START_DATE)
export const addEndDate = createAction(ACTIONS.ADD_END_DATE)
export const addUser = createAction(ACTIONS.ADD_USER)
export const addHotel = createAction(ACTIONS.ADD_HOTEL)
export const addRestaurant = createAction(ACTIONS.ADD_RESTAURANT)
export const addAttraction = createAction(ACTIONS.ADD_ATTRACTION)
export const addHotelCost = createAction(ACTIONS.ADD_HOTEL_COST)
export const addRestaurantCost = createAction(ACTIONS.ADD_RESTAURANT_COST)
export const addAttractionCost = createAction(ACTIONS.ADD_ATTRACTION_COST)
export const saveDate = createAction(ACTIONS.SAVE_DATE)
export const saveTripName = createAction(ACTIONS.SAVE_TRIP_NAME)
export const saveTrip = createAction(ACTIONS.SAVE_TRIP)
export const replaceUsers = createAction(ACTIONS.REPLACE_USERS)
export const listPastVacations = createAction(ACTIONS.LIST_PAST_VACATIONS)

export const loadUsersFromServer = () => dispatch => {
  loadUsers().then(user => {
    dispatch(replaceUsers({ user }))
  })
}

export default ACTIONS
