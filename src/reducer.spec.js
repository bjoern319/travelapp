import ACTIONS from './actions'
import reducer from './reducer'

describe('reducer', () => {
  it('always returns a state', () => {
    const state = {
      users: [{ foo: 'bar' }]
    }

    expect(reducer(state, { type: 'foo' })).toBe(state)
    expect(reducer(state, {})).toBe(state)
    expect(reducer(state)).toBe(state)
  })
})

describe(ACTIONS.ADD_USER, () => {
  it('add a single user', () => {
    const state = {}

    const action = {
      type: ACTIONS.ADD_USER,
      payload: {
        index: 1
      }
    }
    expect(reducer(state, action)).toEqual({
      users: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    })
  })
})

describe(ACTIONS.DELETE_USER, () => {
  it('deletes a single user', () => {
    const state = {
      user: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        },
        {
          name: 'Lara',
          tripName: '-',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    }
    const action = {
      type: ACTIONS.DELETE_USER,
      payload: {
        index: 1
      }
    }

    expect(reducer(state, action)).toEqual({
      users: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    })
  })
})

describe(ACTIONS.ADD_USER, () => {
  it('add a single user', () => {
    const state = {}

    const action = {
      type: ACTIONS.ADD_USER,
      payload: {
        index: 1
      }
    }
    expect(reducer(state, action)).toEqual({
      users: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    })
  })
})

describe(ACTIONS.ADD_TRIP_NAME, () => {
  it('adds a tripName', () => {
    const state = {
      user: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        },
        {
          name: 'Lara',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    }
    const action = {
      type: ACTIONS.ADD_TRIP_NAME,
      payload: {
        index: 0
      }
    }

    expect(reducer(state, action)).toEqual({
      users: [
        {
          name: 'Lewis',
          tripName: 'Asien',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    })
  })
})

describe(ACTIONS.ADD_DESTINATION_NAME, () => {
  it('add a destinationName', () => {
    const state = {
      user: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        },
        {
          name: 'Lara',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    }

    const action = {
      type: ACTIONS.ADD_DESTINATION_NAME,
      payload: {
        index: 0
      }
    }
    expect(reducer(state, action)).toEqual({
      users: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: 'Bangkok',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    })
  })
})
describe(ACTIONS.ADD_BUDGET, () => {
  it('add a destinationName', () => {
    const state = {
      user: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        },
        {
          name: 'Lara',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    }

    const action = {
      type: ACTIONS.ADD_BUDGET,
      payload: {
        index: 0
      }
    }
    expect(reducer(state, action)).toEqual({
      users: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '3000',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    })
  })
})

describe(ACTIONS.ADD_NUMBER_OF_NIGHTS, () => {
  it('add a destinationName', () => {
    const state = {
      user: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        },
        {
          name: 'Lara',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    }

    const action = {
      type: ACTIONS.ADD_NUMBER_OF_NIGHTS,
      payload: {
        index: 0
      }
    }
    expect(reducer(state, action)).toEqual({
      users: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '10',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    })
  })
})
describe(ACTIONS.ADD_HOTEL, () => {
  it('add a hotel', () => {
    const state = {
      user: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        },
        {
          name: 'Lara',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    }

    const action = {
      type: ACTIONS.ADD_HOTEL,
      payload: {
        index: 0
      }
    }
    expect(reducer(state, action)).toEqual({
      users: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: 'Lebua',
          restaurants: '-',
          attractions: '-'
        }
      ]
    })
  })
})
describe(ACTIONS.ADD_RESTAURANT, () => {
  it('add a destinationName', () => {
    const state = {
      user: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        },
        {
          name: 'Lara',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    }

    const action = {
      type: ACTIONS.ADD_DESTINATIONNAME,
      payload: {
        index: 0
      }
    }
    expect(reducer(state, action)).toEqual({
      users: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: 'Lung Ja',
          attractions: '-'
        }
      ]
    })
  })
})
describe(ACTIONS.ADD_ATTRACTION, () => {
  it('add an attraction', () => {
    const state = {
      user: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        },
        {
          name: 'Lara',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    }

    const action = {
      type: ACTIONS.ADD_ATTRACTION,
      payload: {
        index: 0
      }
    }
    expect(reducer(state, action)).toEqual({
      users: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: 'Golden Mountain'
        }
      ]
    })
  })
})

describe(ACTIONS.DELETE_USER, () => {
  it('deletes a single user', () => {
    const state = {
      user: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        },
        {
          name: 'Lara',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    }
    const action = {
      type: ACTIONS.DELETE_USER,
      payload: {
        index: 1
      }
    }

    expect(reducer(state, action)).toEqual({
      users: [
        {
          name: 'Lewis',
          tripName: '-',
          destinationName: '-',
          Budget: '-',
          numberOfNights: '-',
          hotel: '-',
          restaurants: '-',
          attractions: '-'
        }
      ]
    })
  })
})
