import React from 'react'
import ReactDOM from 'react-dom'
import App from './components/App'
import registerServiceWorker from './registerServiceWorker'

import { injectGlobal } from 'styled-components'

injectGlobal`
  * {
    box-sizing: border-box;
  }

  h1 {
    text-align: center; 
  }
  body {
    margin: 0;
    padding: 10px;
    font-family: sans-serif;
    background: #4DB6AC;
    color: white;  
  }
`

ReactDOM.render(<App />, document.getElementById('root'))
registerServiceWorker()
