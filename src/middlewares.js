import { save } from './services'

export const saveToLocalStorage = store => next => action => {
  let result = next(action)
  save('app', store.getState())
  return result
}

export function loadUsers() {
  return fetch('http://localhost:3001/users')
    .then(res => res.json())
    .catch(() => {
      return []
    })
}
