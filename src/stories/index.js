import React from 'react'

import { storiesOf } from '@storybook/react'
import { action } from '@storybook/addon-actions'
import { linkTo } from '@storybook/addon-links'

import { Button, Welcome } from '@storybook/react/demo'
import TripNameInput from '../components/StartScreenComponents/TripNameInput'
import DestinationNameInput from '../components/StartScreenComponents/DestinationNameInput'
import BudgetInput from '../components/StartScreenComponents/BudgetInput'
import NumberOfNightsInput from '../components/StartScreenComponents/NumberOfNightsInput'

import AttractionInput from '../components/CurrentTripScreenComponents/AttractionInput'

storiesOf('TripNameInput', module).add('tripName', () => (
  <TripNameInput onSubmit={action('submitted')}>TripName</TripNameInput>
))

storiesOf('destinationNameInput', module).add('destinationName', () => (
  <DestinationNameInput onSubmit={action('submitted')}>
    DestinationName
  </DestinationNameInput>
))

storiesOf('BudgetInput', module).add('budget', () => (
  <BudgetInput onSubmit={action('submitted')}>budget</BudgetInput>
))

storiesOf('NumberOfNightsInput', module).add('numberOfNights', () => (
  <NumberOfNightsInput onSubmit={action('submitted')}>
    numberOfNights
  </NumberOfNightsInput>
))

storiesOf('AttractionInput', module).add('attraction', () => (
  <AttractionInput onSubmit={action('submitted')}>attraction</AttractionInput>
))

storiesOf('CostInput', models).add('cost', () => (
  <CostInput onSubmit={action('submitted')}>cost</CostInput>
))

storiesOf('HotelInput', modules).add('hotel', () => (
  <HotelInput onSubmit={action('submitted')}>hotel</HotelInput>
))

storiesOf('RestaurantInput', modules).add('restaurant', () => (
  <RestaurantInput onSubmit={action('submitted')}>restaurant</RestaurantInput>
))

storiesOf('Welcome', module).add('to Storybook', () => (
  <Welcome showApp={linkTo('Button')} />
))

storiesOf('Button', module)
  .add('with text', () => (
    <Button onClick={action('clicked')}>Hello Button</Button>
  ))
  .add('with some emoji', () => (
    <Button onClick={action('clicked')}>😀 😎 👍 💯</Button>
  ))
