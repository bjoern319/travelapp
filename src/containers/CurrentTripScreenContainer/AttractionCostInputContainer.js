import { addAttractionCost } from '../../actions'
import { connect } from 'react-redux'
import CostInput from '../../components/CurrentTripScreenComponents/CostInput'

const mapStateToProps = state => ({
  placeholder: 'add cost'
})

const mapDispatchToProps = dispatch => ({
  onSubmit: attractionCost => dispatch(addAttractionCost({ attractionCost }))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CostInput)
