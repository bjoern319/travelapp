import { connect } from 'react-redux'
import TripCardHeader from '../../components/CurrentTripScreenComponents/TripCardHeader'

const mapStateToProps = state => ({
  tripName: state.tripName,
  destinationName: state.destinationName,
  budget: state.budget,
  numberOfNights: state.numberOfNights,
  hotel: state.hotel,
  restaurant: state.restaurant,
  attraction: state.attraction,
  hotelCost: state.hotelCost,
  restaurantCost: state.restaurantCost,
  attracttionCost: state.attractionCost,
  restBudget: state.restBudget
})

export default connect(
  mapStateToProps,
  null
)(TripCardHeader)
