import { addRestaurantCost } from '../../actions'
import { connect } from 'react-redux'
import CostInput from '../../components/CurrentTripScreenComponents/CostInput'

const mapStateToProps = state => ({
  placeholder: 'add cost'
})

const mapDispatchToProps = dispatch => ({
  onSubmit: restaurantCost => dispatch(addRestaurantCost({ restaurantCost }))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CostInput)
