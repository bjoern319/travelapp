import { connect } from 'react-redux'
import CurrentTripScreen from '../../screens/CurrentTripScreen'

const mapStateToProps = state => ({
  attraction: state.attraction,
  hotel: state.hotel,
  restaurant: state.restaurant,
  hotelCost: state.hotelCost,
  restaurantCost: state.restaurantCost,
  attractionCost: state.attractionCost,
  restBudget: state.restBudget
})

export default connect(
  mapStateToProps,
  null
)(CurrentTripScreen)
