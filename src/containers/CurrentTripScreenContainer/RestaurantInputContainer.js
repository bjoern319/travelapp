import { addRestaurant } from '../../actions'
import { connect } from 'react-redux'
import RestaurantInput from '../../components/CurrentTripScreenComponents/RestaurantInput'

const mapStateToProps = state => ({
  placeholder: 'add a restaurant'
})

const mapDispatchToProps = dispatch => ({
  onSubmit: restaurant => dispatch(addRestaurant({ restaurant }))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RestaurantInput)
