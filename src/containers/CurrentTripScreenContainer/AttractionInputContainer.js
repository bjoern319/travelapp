import { addAttraction } from '../../actions'
import { connect } from 'react-redux'
import AttractionInput from '../../components/CurrentTripScreenComponents/AttractionInput'

const mapStateToProps = state => ({
  placeholder: 'add an attraction'
})

const mapDispatchToProps = dispatch => ({
  onSubmit: attraction => dispatch(addAttraction({ attraction }))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AttractionInput)
