import { addHotel } from '../../actions'
import { connect } from 'react-redux'
import HotelInput from '../../components/CurrentTripScreenComponents/HotelInput'

const mapStateToProps = state => ({
  placeholder: 'add a Hotel'
})

const mapDispatchToProps = dispatch => ({
  onSubmit: hotel => dispatch(addHotel({ hotel }))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HotelInput)
