import { addHotelCost } from '../../actions'
import { connect } from 'react-redux'
import CostInput from '../../components/CurrentTripScreenComponents/CostInput'

const mapStateToProps = state => ({
  placeholder: 'add cost'
})

const mapDispatchToProps = dispatch => ({
  onSubmit: hotelCost => dispatch(addHotelCost({ hotelCost }))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CostInput)
