import { connect } from 'react-redux'
import StartScreen from '../../screens/StartScreen'

const mapStateToProps = state => ({
  user: state.user,
  tripName: state.tripName,
  destinationName: state.destinationName,
  budget: state.budget,
  numberOfNights: state.numberOfNights,
  restBudget: state.restBudget
})

export default connect(
  mapStateToProps,
  null
)(StartScreen)
