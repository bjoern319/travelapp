import { connect } from 'react-redux'
import { addDestinationName } from '../../actions'
import DestinationNameInput from '../../components/StartScreenComponents/DestinationNameInput'

const mapStateToProps = state => ({
  placeholder: 'Destination'
})

const mapDispatchToProps = dispatch => ({
  onSubmit: destinationName => dispatch(addDestinationName({ destinationName }))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DestinationNameInput)
