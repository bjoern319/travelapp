import { addUser } from '../../actions'
import { connect } from 'react-redux'
import Input from '../../components/Input'

const mapStateToProps = state => ({
  placeholder: 'user'
})

const mapDispatchToProps = dispatch => ({
  onSubmit: user => dispatch(addUser({ user }))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Input)
