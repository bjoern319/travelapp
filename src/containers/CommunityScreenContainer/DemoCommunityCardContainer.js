import { connect } from 'react-redux'
import DemoCommunityCard from '../../components/CommunityScreenComponents/DemoCommunityCard'

const mapStateToProps = state => ({
  state: state
})

export default connect(
  mapStateToProps,
  null
)(DemoCommunityCard)
