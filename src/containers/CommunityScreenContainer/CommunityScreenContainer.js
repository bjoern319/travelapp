import { connect } from 'react-redux'
import CommunityScreen from '../../screens/CommunityScreen'

const mapStateToProps = state => ({
  tripName: state.tripName,
  destinationName: state.destinationName,
  budget: state.budget,
  numberOfNights: state.numberOfNights,
  hotels: state.hotels,
  hotel: state.hotel,
  restaurants: state.restaurants,
  restaurant: state.restaurant,
  attractions: state.attractions,
  attraction: state.attraction
})

export default connect(
  mapStateToProps,
  null
)(CommunityScreen)
