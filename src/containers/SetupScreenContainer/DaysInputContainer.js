import { connect } from 'react-redux'
import { addNumberOfNights } from '../../actions'
import NumberOfNightsInput from '../../components/StartScreenComponents/NumberOfNightsInput'

const mapStateToProps = state => ({
  placeholder: 'number of nights'
})

const mapDispatchToProps = dispatch => ({
  onSubmit: numberOfNights => dispatch(addNumberOfNights({ numberOfNights }))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NumberOfNightsInput)
