import { connect } from 'react-redux'
import { addBudget } from '../../actions'
import BudgetInput from '../../components/StartScreenComponents/BudgetInput'

const mapStateToProps = state => ({
  placeholder: 'Budget in €'
})

const mapDispatchToProps = dispatch => ({
  onSubmit: budget => dispatch(addBudget({ budget }))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BudgetInput)
