import { addTripName } from '../../actions'
import { connect } from 'react-redux'
import TripNameInput from '../../components/StartScreenComponents/TripNameInput'

const mapStateToProps = state => ({
  placeholder: 'Trip name'
})

const mapDispatchToProps = dispatch => ({
  onSubmit: tripName => dispatch(addTripName({ tripName }))
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TripNameInput)
