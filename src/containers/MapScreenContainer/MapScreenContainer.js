import { connect } from 'react-redux'
import MapScreen from '../../screens/MapScreen'

const mapStateToProps = state => ({
  destinationName: state.destinationName,
  user: state.user
})

export default connect(
  mapStateToProps,
  null
)(MapScreen)
