import { connect } from 'react-redux'
import Map from '../../components/MapScreenComponents/Map'

const mapStateToProps = state => ({
  destinationName: state.destinationName,
  user: state.user
})

export default connect(
  mapStateToProps,
  null
)(Map)
