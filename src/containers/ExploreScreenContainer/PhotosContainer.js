import { connect } from 'react-redux'
import Photos from '../../components/ExploreScreenComponents/Photos'

const mapStateToProps = state => ({
  destinationName: state.destinationName,
  user: state.user
})

export default connect(
  mapStateToProps,
  null
)(Photos)
