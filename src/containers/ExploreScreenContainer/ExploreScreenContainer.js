import { connect } from 'react-redux'
import ExploreScreen from '../../screens/ExploreScreen'

const mapStateToProps = state => ({
  destinationName: state.destinationName,
  user: state.user
})

export default connect(
  mapStateToProps,
  null
)(ExploreScreen)
