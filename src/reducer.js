import ACTIONS from './actions'
import { load } from './services'

const initialState = load('app') || {
  tripName: load('tripName') || null,
  destinationName: load('destinationName') || null,
  budget: load('budget') || null,
  user: load('user') || null,
  numberOfNights: null,
  hotel: load('hotel') || null,
  restaurant: load('restaurant') || null,
  attraction: load('attraction') || null,
  hotelCost: load('hotelCost') || null,
  restaurantCost: load('restaurantCost') || null,
  attractionCost: load('attractionCost') || null,
  restBudget: load('restBudget') || null,
  trip: {
    userName: load('userName') || null,
    tripName: load('tripName') || null,
    destinationName: load('destinationName') || null,
    restBudget: load('restBudget') || null,
    numberOfNights: load('numberOfNights') || null,
    budget: load('budget') || null,
    hotels: load('hotels') || [{ hotelName: null, hotelCost: null }],
    restaurants: load('restaurants') || [
      { restaurantName: null, restaurantCost: null }
    ],
    attractions: load('attractions') || [
      { attractionName: null, attractionCost: null }
    ],
    packingListItem: load('packingListItem') || [{ itemName: null }],
    previousTrips: load('reviousTrips') || []
  }
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case ACTIONS.ADD_USER:
      return {
        ...state,
        user: action.payload.user,
        trip: {
          ...state.trip,
          userName: action.payload.user
        }
      }
    case ACTIONS.DELETE_USER:
      return {
        ...state,
        user: null,
        trip: {
          ...state.trip,
          user: null
        }
      }
    case ACTIONS.ADD_TRIP_NAME:
      return {
        ...state,
        tripName: action.payload.tripName,
        trip: {
          ...state.tripName,
          tripName: action.payload.tripName
        }
      }
    case ACTIONS.ADD_DESTINATION_NAME:
      return {
        ...state,
        destinationName: action.payload.destinationName,
        trip: {
          ...state.trip,
          destinationName: action.payload.destinationName
        }
      }
    case ACTIONS.ADD_BUDGET:
      return {
        ...state,
        budget: action.payload.budget,
        restBudget: action.payload.budget,
        trip: {
          ...state.trip,
          budget: action.payload.budget,
          restBudget: action.payload.budget
        }
      }
    case ACTIONS.ADD_NUMBER_OF_NIGHTS:
      return {
        ...state,
        numberOfNights: action.payload.numberOfNights,
        trip: {
          numberOfNights: action.payload.numberOfNights
        }
      }
    case ACTIONS.ADD_HOTEL:
      return {
        ...state,
        hotel: action.payload.hotel,
        trip: {
          ...state.trip,
          hotels: [{ ...state.hotelName, hotelName: action.payload.hotel }]
        }
      }
    case ACTIONS.ADD_RESTAURANT:
      return {
        ...state,
        restaurant: action.payload.restaurant,
        trip: {
          ...state.trip,
          restaurants: [
            {
              ...state.restaurantName,
              restaurantName: action.payload.restaurant
            }
          ]
        }
      }
    case ACTIONS.ADD_ATTRACTION:
      return {
        ...state,
        attraction: action.payload.attraction,
        trip: {
          ...state.trip,
          attractions: [
            {
              ...state.attractionName,
              attractionName: action.payload.attraction
            }
          ]
        }
      }
    case ACTIONS.ADD_HOTEL_COST:
      return {
        ...state,
        hotelCost: action.payload.hotelCost,
        restBudget: state.restBudget - action.payload.hotelCost,
        trip: {
          ...state.trip,
          hotels: [
            {
              ...state.hotelCost,
              hotelCost: action.payload.hotelCost
            }
          ]
        }
      }
    case ACTIONS.ADD_RESTAURANT_COST:
      return {
        ...state,
        restaurantCost: action.payload.restaurantCost,
        restBudget: state.restBudget - action.payload.restaurantCost,
        trip: {
          ...state.trip,
          restaurants: [
            {
              ...state.restaurantCost,
              restaurantCost: action.payload.restaurantCost
            }
          ]
        }
      }
    case ACTIONS.ADD_ATTRACTION_COST:
      return {
        ...state,
        attractionCost: action.payload.attractionCost,
        restBudget: state.restBudget - action.payload.attractionCost,
        trip: {
          ...state.trip,
          attractions: [
            {
              ...state.attractionCost,
              attractionCost: action.payload.attractionCost
            }
          ]
        }
      }

    default:
      return state
  }
}
